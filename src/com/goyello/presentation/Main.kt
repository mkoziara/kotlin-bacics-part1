package com.goyello.presentation

import com.goyello.presentation.divider.SwearWordDivider
import com.goyello.presentation.files.SwearFileReader
import com.goyello.presentation.model.Word
import com.goyello.presentation.statistics.WordStatisticsPrinter


fun main(args: Array<String>) {

    val filePath: String? = args.getOrNull(0)
    val lazyFile = filePath?.let { SwearFileReader().readTextFromFile(it) } ?: throw RuntimeException()

    // todo null safety example
//    if (filePath != null) {
//        val textWithSwears = SwearFileReader().readTextFromFile(filePath)
//    } else {
//        throw RuntimeException()
//    }

    // todo initial
//    val textWithSwears = SwearFileReader().readTextFromFile()
//    println(textWithSwears)

    println(lazyFile.fileContent)
    println(lazyFile.fileContent)
    println(lazyFile.fileContent)

    val swearText = lazyFile.fileContent
    val devidedWords: List<Word> = SwearWordDivider().divideWords(swearText)
    val cleanText = devidedWords.joinToString(separator = " ", transform = Word::getWordValue)
    println(cleanText)

    WordStatisticsPrinter().printStatistics(devidedWords)


    /* todo przykład extentions */
//    val stringExtention: String = "Koziara "
//    println(stringExtention.getMaciek())

    /* todo przykład copy */
//    val normalWord: NormalWord = NormalWord("test", "koziara")
//    println(normalWord)
//
//    val secondWord = normalWord.copy(word = "Maciek")
//    println(normalWord)
//    println(secondWord)


    /* todo przykład object*/
//    val swearWord1 = SwearWord
//    val swearWord2 = SwearWord
//    val swearWord3 = SwearWord
//
//    println(""" $swearWord1
//                $swearWord2
//                $swearWord3 """)

    /*todo delegation example*/
//    val betterSwearWord = BetterSwearWord("realWord")
//    println(betterSwearWord.realWord)
//    println(betterSwearWord.getWordValue())

}