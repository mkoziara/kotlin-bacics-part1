package com.goyello.presentation.statistics

import com.goyello.presentation.model.BetterSwearWord
import com.goyello.presentation.model.NormalWord
import com.goyello.presentation.model.SwearWord
import com.goyello.presentation.model.Word

class WordStatisticsPrinter {

    fun printStatistics(words: List<Word>) {

        val swearWords = words.count { it is SwearWord }
        val normalWords = words.count { it is NormalWord }

        println("""Normal words - $normalWords, swearWords - $swearWords""")

        val countedWords = words
                .groupBy {if (it is BetterSwearWord) it.realWord else it.getWordValue()}
                .mapValues { (_, value) -> value.size }
        countedWords.forEach { word, count -> println("""$word - $count""") }
    }
}