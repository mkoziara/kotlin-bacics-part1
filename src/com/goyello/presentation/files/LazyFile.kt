package com.goyello.presentation.files

import java.io.File

class LazyFile(file: File) {

    val fileContent: String by lazy {
        println("reading file")
        file.readText()
    }
}