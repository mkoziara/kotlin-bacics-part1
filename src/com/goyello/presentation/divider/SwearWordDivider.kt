package com.goyello.presentation.divider

import com.goyello.presentation.model.BetterSwearWord
import com.goyello.presentation.model.NormalWord
import com.goyello.presentation.model.SwearWord
import com.goyello.presentation.model.Word

class SwearWordDivider {

    private val swearWords: Array<String> = arrayOf("fucking", "fucked", "fuck", "sucked", "cunt")

    fun divideWords(swearText: String): List<Word> {
        return swearText
                .split(" ")
//                .map({s -> if (swearWords.contains(s)) NormalWord(s) else SwearWord })
//                .map({if (swearWords.contains(it)) NormalWord(it) else SwearWord })
                .map { if (swearWords.contains(it)) BetterSwearWord(it) else NormalWord(it) }
    }
}