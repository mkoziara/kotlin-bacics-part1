package com.goyello.presentation.model

interface Word {
    fun getWordValue(): String
}