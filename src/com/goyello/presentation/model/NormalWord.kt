package com.goyello.presentation.model

data class NormalWord(val word: String) : Word {

    override fun getWordValue(): String = word
}