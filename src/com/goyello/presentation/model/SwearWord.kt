package com.goyello.presentation.model

object SwearWord : Word {

    override fun getWordValue() = "***"

}